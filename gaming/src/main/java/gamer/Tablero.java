/*
 *Copyright 2021 Javier da Silva Licensed under the Apache License, Version 2.0 (the "License");you may not use this file except in compliance with the License.You may obtain a copy of the License athttp://www.apache.org/licenses/LICENSE-2.0Unless required by applicable law or agreed to in writing,software distributed under the License is distributed on an"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied. See the License for the specificlanguage governing permissions and limitations under theLicense.
 */ 


package gamer;

//En esta clase se lee el tablero de un fichero en forma de 0 y 1, ir transitando de estados e ir mostrando dichos estados.
public class Tablero{

	//Dimension del tablero.
	private static int DIMENSION = 30;
	//Matriz que representa el estado actual.
	private int[][] estadoActual = new int[DIMENSION][DIMENSION];
	//Matriz que representa el estado siguiente.
	private int[][] estadoSiguiente = new int[DIMENSION][DIMENSION];

	public void leerEstadoActual(){
		//Todo par de "for" hace la función de ir por cada posición, es decir, 'i' sería la 'x' y 'j' sería la 'y', e iría recorriendo todo el tablero hasta DIMENSION. 
		for (int i = 0; i < DIMENSION; i++){
			for(int j = 0; j < DIMENSION; j++){
				double generador = Math.random()*(1)+(-1);
				if(generador <= 0.5){//Simple 50/50 para los 1 y 0.
					estadoActual[i][j] = 1;
				}
				else estadoActual[i][j] = 0;
			
			}
		}
	}

	public void generarEstadoActualPorMontecarlo(){
		//Mismo par de for
		for(int i=0; i < DIMENSION; i++){
			for(int j = 0; j < DIMENSION; j++){
			int vecinos = 0;
			//Vamos a saber los vecinos comprobando sus cuadrantes con -1 y más 1 como rango (ya que rodea al valor).
			for (int m = -1; m<=1; m++){
				for(int n = -1; n <= 1; n++){
					vecinos = vecinos + estadoActual[i+m][j+n];	
				}
			} 
			}
		}
		vecinos = vecinos - estadoActual[i+m][j+n] //en el par de for anterior como se pasa por n=0 y m=0 se borra el estado actual para no añadir mas vecinos de la cuenta
		if(estadoActual[i][j] == 1){
			switch(vecinos){
			//Como solo se mantiene vivo si hay 2 o 3 vecinos simplemente añadimos este case y el resto son 0
			//el case 2 y 3 serian la regla de "Si tiene 2 o 3 vecinos sobrevive la generacion"
			//El resto son "Si tiene menos de 2 vecinos muere por falta de poblacion y si tiene mas de 4 vecinos es sobrepoblacion"
			case 2:
				estadoSiguiente[i][j] = 1;
				break;
			case 3:
				estadoSiguiente[i][j] = 1;
				break;
			default:
				estadoSiguiente[i][j] = 0;

				}

			}
			else{ //else porque solo puede ser o 1 o 0
			switch(vecinos){
				//Aquí se implementa de que si hay 3 vecinos exacto se crea uno nuevo en el 0
			case 3:
				estadoSiguiente[i][j] = 1;
				break;
			default:
				estadoSiguiente[i][j] = 0;
			}
		}

}
	
	public void transitarAlEstadoSiguiente(){
		estadoActual[][] = estadoSiguiente[][];
		estadoSiguiente[][] = new int [DIMENSION][DIMENSION];
		System.out.println(estadoActual.toString());
	}

	//La funcion del toString() es poder convertir estadoActual que es un grid a texto yendo coordenada a coordenada y añadiendo al string el valor de cada una pero añadiendo el salto de linea (\n) cada vez que se llegue al final de j	
	@Override
	public String toString(){
	String tableroActual = "";
	for(int i = 0; i < DIMENSION; i++){
		for(int j = 0; j < DIMENSION; j++){
			if(estadoActual[i][j] == 1) tableroActual += "1";
			else tableroActual += "0";
		}
		tableroActual += "\n";

	}	
	return tableroActual;
	}	
}
